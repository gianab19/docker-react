FROM node:16-alpine as matteo

WORKDIR /app

COPY package.json .
RUN npm install
COPY . .

RUN npm run build

FROM nginx
COPY --from=matteo /app/build /usr/share/nginx/html